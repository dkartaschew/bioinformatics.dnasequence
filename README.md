DNA Sequence Representation Library for Bioinformatics
======================================================
----
Generic DNA Sequence library for desktop client and related tools


Install
-------------

* run **$ mvn clean install** to build.

Using
-------------

Include the library in your POM file.