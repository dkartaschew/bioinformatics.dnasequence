package au.edu.qut.bioinformatics.dnasequence;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Iterator;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class DNASequenceTest {

    /**
     * Testing filename.
     */
    private String filename = "/AE015929.gbk";
    /**
     * Testing sequence.
     */
    private DNASequence sequence = null;

    @Before
    public void DNASequenceTests() throws FileNotFoundException, DNASequenceException {
        sequence = new DNASequence(new File(this.getClass().getResource(filename).getFile()));
    }

    /**
     * Test that the sequence length is what we are expecting.
     */
    @Test
    public void testSequenceLength() {
        assertEquals(sequence.getDNASequenceLength(), 2499279);
    }

    /**
     * Test the filename is recorded correctly.
     */
    @Test
    public void testSequenceFilename() {
        assertEquals(sequence.getFilename(), this.getClass().getResource(filename).getFile());
    }

    /**
     * Test to ensure the name is read correctly.
     */
    @Test
    public void testSequenceOrganism() {
        assertEquals(sequence.getOrganismName(), "Staphylococcus epidermidis ATCC 12228");
    }

    /**
     * Test that the first 10mer is read correctly
     */
    @Test
    public void testFirst10mer() throws DNASequenceException {
        assertEquals(new String(sequence.getKMer(10, 0)), "aagaaattgt");
    }

    /**
     * Test for 0mer lengths
     */
    @Test(expected = DNASequenceException.class)
    public void testFirst0mer() throws DNASequenceException {
        char[] result = new char[] {'a'};
        assertTrue(Arrays.equals(sequence.getKMer(0, 0), result));
        fail();
    }

    /**
     * Test for negative mer lengths
     */
    @Test(expected = DNASequenceException.class)
    public void testFirstNegmer() throws DNASequenceException {
        assertEquals(sequence.getKMer(-1, 0), "aagaaattgt");
    }

    /**
     * Test for negative offset into the sequence.
     */
    @Test(expected = DNASequenceException.class)
    public void testFirstNegOffset() throws DNASequenceException {
        assertEquals(sequence.getKMer(1, -1), "aagaaattgt");
    }

    /**
     * Test for offset beyond sequence length
     */
    @Test(expected = DNASequenceException.class)
    public void testFirstHugeOffset() throws DNASequenceException {
        assertEquals(sequence.getKMer(1, Integer.MAX_VALUE), "aagaaattgt");
    }

    /**
     * Test for offset beyond sequence length
     */
    @Test(expected = DNASequenceException.class)
    public void testFirstHugeOffset2() throws DNASequenceException {
        assertEquals(sequence.getKMer(1, sequence.getDNASequenceLength()), "aagaaattgt");
    }

    /**
     * Test for offset beyond sequence length
     */
    @Test(expected = DNASequenceException.class)
    public void testFirstHugeOffset3() throws DNASequenceException {
        assertEquals(sequence.getKMer(1, sequence.getDNASequenceLength() + 1), "aagaaattgt");
    }

    /**
     * Test for k length greater than sequence length.
     */
    @Test(expected = DNASequenceException.class)
    public void testFirstHugeK() throws DNASequenceException {
        assertEquals(sequence.getKMer(Integer.MAX_VALUE, 0), "aagaaattgt");
    }

    /**
     * Test for k length greater than sequence length.
     */
    @Test(expected = DNASequenceException.class)
    public void testFirstHugeK2() throws DNASequenceException {
        assertEquals(sequence.getKMer(sequence.getDNASequenceLength() + 1, 0), "aagaaattgt");
    }
    
    /**
     * Test for functional iterator 
     */
    @Test
    public void testIterator() throws DNASequenceException {
        Iterator<char[]> iter = sequence.iterator(5);
        char[] result = new char[] {'a','a','g','a','a'};
        assertTrue(iter.hasNext());
        assertTrue(Arrays.equals(iter.next(), result));
    }
    
    /**
     * Test for functional iterator 
     */
    @Test
    public void testIterator2() throws DNASequenceException {
        Iterator<char[]> iter = sequence.iterator(5);
        char[] result = new char[] {'a','g','a','a', 'a'};
        assertTrue(iter.hasNext());
        iter.next();    // skip the first one.
        assertTrue(iter.hasNext());
        assertTrue(Arrays.equals(iter.next(), result));
    }
    
    /**
     * Test for functional iterator of different length
     */
    @Test
    public void testIterator3() throws DNASequenceException {
        Iterator<char[]> iter = sequence.iterator(5);
        char[] result = new char[] {'g','a','a', 'a', 't'};
        assertTrue(iter.hasNext());
        iter.next();    // skip the first one.
        iter.next();    // skip the second one.
        assertTrue(iter.hasNext());
        assertTrue(Arrays.equals(iter.next(), result));
    }
    
    /**
     * Test for functional iterator of different length
     */
    @Test(expected = DNASequenceException.class)
    public void testIteratorFail0Length() throws DNASequenceException {
        Iterator<char[]> iter = sequence.iterator(0);
        fail();
    }
    
    /**
     * Test for functional iterator of different length
     */
    @Test(expected = DNASequenceException.class)
    public void testIteratorFailNegLength() throws DNASequenceException {
        Iterator<char[]> iter = sequence.iterator(-1);
        fail();
    }
    
    /**
     * Test for functional iterator of different length
     */
    @Test(expected = DNASequenceException.class)
    public void testIteratorFailMaxNegLength() throws DNASequenceException {
        Iterator<char[]> iter = sequence.iterator(Integer.MIN_VALUE);
        fail();
    }
    
    /**
     * Test for functional iterator of different length
     */
    @Test(expected = DNASequenceException.class)
    public void testIteratorFailMaxPosLength() throws DNASequenceException {
        Iterator<char[]> iter = sequence.iterator(Integer.MAX_VALUE);
        fail();
    }
    
    /**
     * Test for functional iterator of different length
     */
    @Test
    public void testIteratorSequenceLength() throws DNASequenceException {
        Iterator<char[]> iter = sequence.iterator(sequence.getDNASequenceLength());
        assertTrue(iter.hasNext());
        iter.next();    // skip the first one.
        assertFalse(iter.hasNext()); // should only have 1.
    }
    
    /**
     * Test for functional iterator of different length
     */
    @Test
    public void testIteratorSequenceLength2() throws DNASequenceException {
        Iterator<char[]> iter = sequence.iterator(sequence.getDNASequenceLength()-1);
        assertTrue(iter.hasNext());
        iter.next();    // skip the first one.
        iter.next();    // skip the first one.
        assertFalse(iter.hasNext()); // should only have 2.
    }
    
    /**
     * Test for functional iterator of different length
     */
    @Test(expected = DNASequenceException.class)
    public void testIteratorFailSequenceLength() throws DNASequenceException {
        Iterator<char[]> iter = sequence.iterator(sequence.getDNASequenceLength()+1);
        fail();
    }
}
