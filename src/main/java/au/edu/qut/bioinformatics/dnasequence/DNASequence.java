/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.dnasequence;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 * Class to hold a raw DNA sequence and to extract k-mers from the sequence.
 *
 * @author Darran Karatschew
 */
public class DNASequence {

    /**
     * File extension for GenBank files.
     */
    private final String GBKFILE = ".gbk";
    /**
     * Field name that includes the number of base pairs for the genome.
     */
    private final String LOCUS = "LOCUS";
    /**
     * The offset of the base pair count on the LOCUS line.
     */
    private final int BASEPAIROFFSET = 3;
    /**
     * Field name for the organism name.
     */
    private final String ORGANISMNAME = "SOURCE      ";
    /**
     * Marker for the start of the raw dna sequence.
     */
    private final String ORIGIN = "ORIGIN";
    /**
     * The filename from GenBank
     */
    private final String filename;
    /**
     * The organismName;
     */
    private final String organismName;
    /**
     * The DNA sequence for this organism
     */
    private final char[] dnaSequence;

    /**
     * Extract a DNA sequence from the file specified.
     *
     * @param file The file to extract the DNA information from.
     */
    public DNASequence(File file) throws FileNotFoundException, DNASequenceException {

        // Check basic file parameters.
        if (!file.exists() || !file.canRead() || file.isDirectory()) {
            throw new DNASequenceException("The file does not appear to exist or to be readable.");
        }
        // Check if the file has the correct file extension.
        if (!file.getName().toLowerCase().endsWith(GBKFILE)) {
            throw new DNASequenceException("The file does not appear to be a GenBank file (incorrect file extension).");
        }
        filename = file.getAbsolutePath();

        Scanner in = new Scanner(file);

        // Scan for the locus definition. This line has multiple fields of interest. We are only after the base pair count.
        String[] items = split(findLine(in, LOCUS, "Unable to locate the LOCUS field."), " ");
        if (items.length < BASEPAIROFFSET) {
            throw new DNASequenceException("The LOCUS field does not contain the number of base pairs.");
        }

        // Create the array to hold our base pairs.
        try {
            dnaSequence = new char[(Integer.parseInt(items[BASEPAIROFFSET - 1]))];
        } catch (NumberFormatException e) {
            throw new DNASequenceException("The number of base pairs in the file appears to be invalid. Please check the file format is correct.");
        }

        // Scan for the organism name definition
        organismName = findLine(in, ORGANISMNAME, "Unable to locate the SOURCE field.").substring(ORGANISMNAME.length());

        // Scan for the raw dna sequence.
        findLine(in, ORIGIN, "Unable to locate the ORIGIN field. Missing raw DNA sequence to process");
        // line = ORIGIN
        getDNASequence(in, dnaSequence);
        
        in.close();
    }

    /**
     * Scan for a line starting with the defined prefix. Return the line with the prefix.
     *
     * @param scanner The scanner to use.
     * @param linePrefix The prefix of the line to scan for.
     * @param exceptionMessage The message to give if the EOF is found before encountering the prefix
     * @return The line which contains the prefix.
     * @throws DNASequenceException If a line with the prefix is not found.
     */
    private String findLine(Scanner scanner, String linePrefix, String exceptionMessage) throws DNASequenceException {
        // Continue looking for the line, until we either find it, or run out of file to scan.
        String line = scanner.nextLine();
        while (!line.startsWith(linePrefix)) {
            if (scanner.hasNextLine()) {
                line = scanner.nextLine();
            } else {
                // Not found due to end of file, so throw an exception.
                throw new DNASequenceException(exceptionMessage);
            }
        }
        return line;
    }

    /**
     * Read the file for DNA sequence information and store in the provided character array.
     *
     * @param in The input stream to read.
     * @param dnaSequence The location to store the DNA sequence.
     * @throws DNASequenceException The DNA sequence being read is longer then the number of pair specified in the LOCUS
     * field.
     */
    private void getDNASequence(Scanner in, char[] dnaSequence) throws DNASequenceException {
        String line = in.nextLine();
        int dnaSequenceOffset = 0;
        while (!line.equals("//") && in.hasNextLine()) {
            String[] strands = split(line.substring(10), " ");
            for (String strand : strands) {
                if (strand.length() + dnaSequenceOffset > dnaSequence.length) {
                    throw new DNASequenceException("DNA Sequence is longer than the number of base pairs specified in LOCUS");
                }
                System.arraycopy(strand.toCharArray(), 0, dnaSequence, dnaSequenceOffset, strand.length());
                dnaSequenceOffset += strand.length();
            }
            line = in.nextLine();
        }
        if (dnaSequenceOffset != dnaSequence.length) {
            throw new DNASequenceException("DNA Sequence is shorter than the number of base pairs specified in LOCUS");
        }
    }

    /**
     * Split strings removing any empty strings from the result.
     *
     * @param string The string to split
     * @param delimiter The delimiter on which to split.
     * @return An array of Strings.
     */
    private String[] split(String string, String delimiter) {
        String[] items = string.split(delimiter);
        List<String> stringList = new ArrayList<>();
        for (String str : items) {
            if (str != null && str.length() > 0) {
                stringList.add(str);
            }
        }
        return stringList.toArray(new String[stringList.size()]);
    }

    /**
     * Get the filename that was used to extract the information from.
     *
     * @return The filename.
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Get the Organism Name as contained in the GenBank file.
     *
     * @return The Organism Name
     */
    public String getOrganismName() {
        return organismName;
    }

    /**
     * Get the length of the DNA Sequence that this object represents.
     *
     * @return The length of the DNA Sequence.
     */
    public int getDNASequenceLength() {
        return dnaSequence.length;
    }

    /**
     * Get a pointer to the complete DNA sequence.
     *
     * @return The complete DNA sequence. This is a reference to the underlying storage container, so modifications on
     * the return character array will be reflected in further queries.
     */
    public char[] getDNASequence() {
        return dnaSequence;
    }

    /**
     * Get an iterator for kmers for the specified k value
     *
     * @param k The k value to be used to split the dna sequence into.
     * @return An iterator to the sequence of kmers.
     */
    public Iterator<char[]> iterator(int k) throws DNASequenceException {
        return new DNASequencekMerIterator<>(dnaSequence, k);
    }

    /**
     * Retrieve the kmer for value K, at the specified offset.
     *
     * @param k The k value to use for the mer size.
     * @param offset The offset of the kmer into the DNA sequence.
     * @return the kmer value sequence as char[].
     */
    public char[] getKMer(int k, int offset) throws DNASequenceException {
        // check the offsets and k values, and if invlalid return null.
        if (offset + k > dnaSequence.length || k < 1 || offset >= dnaSequence.length || offset < 0) {
            throw new DNASequenceException("k or offset values are invalid for the sequence.");
        }
        char[] kmer = new char[k];
        System.arraycopy(dnaSequence, offset, kmer, 0, k);
        return kmer;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.filename != null ? this.filename.hashCode() : 0);
        hash = 79 * hash + (this.organismName != null ? this.organismName.hashCode() : 0);
        hash = 79 * hash + Arrays.hashCode(this.dnaSequence);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DNASequence other = (DNASequence) obj;
        if ((this.filename == null) ? (other.filename != null) : !this.filename.equals(other.filename)) {
            return false;
        }
        if ((this.organismName == null) ? (other.organismName != null) : !this.organismName.equals(other.organismName)) {
            return false;
        }
        if (!Arrays.equals(this.dnaSequence, other.dnaSequence)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "File: " + filename + " ; Organism: " + organismName + " ; BasePairs: " + dnaSequence.length;
    }
}
