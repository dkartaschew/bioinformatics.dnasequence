/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.dnasequence;

import java.util.Iterator;

/**
 * kMer iterator for a sequence of DNA.
 *
 * @author Darran Kartaschew
 */
public class DNASequencekMerIterator<V> implements Iterator<V> {

    /**
     * The size of the kmer to extract from the stream
     */
    private final int kmerSize;
    /**
     * The raw sequence from which they are extracted.
     */
    private final char[] sequence;
    /**
     * The current offset into the sequence.
     */
    private int offsetIntoSequence;

    /**
     * Create an iterator on kMer based on a dna sequence, with k = x.
     *
     * @param dnaSequence The dna sequence to constructor
     * @param k The k value to apply to the kmer.
     * @throws DNASequenceException If the dna sequence is null or empty, of the k value is negative or greater than the
     * length of the dna sequence.
     */
    public DNASequencekMerIterator(char[] dnaSequence, int k) throws DNASequenceException {
        if (dnaSequence == null || dnaSequence.length == 0) {
            throw new DNASequenceException("DNA Sequence is null or empty?");
        }
        if (k < 1 || k > dnaSequence.length) {
            throw new DNASequenceException("k value is invalid, it is negative or a value greater than the length of the DNA sequence supplied?");
        }
        sequence = dnaSequence;
        kmerSize = k;
        offsetIntoSequence = 0;
    }

    @Override
    public boolean hasNext() {
        return (offsetIntoSequence + kmerSize <= sequence.length);
    }

    @Override
    public V next() {
        if (hasNext()) {
            char[] kmer = new char[kmerSize];
            System.arraycopy(sequence, offsetIntoSequence++, kmer, 0, kmerSize);
            return (V) kmer;
        }
        return null;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
